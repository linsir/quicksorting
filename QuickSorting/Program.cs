﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickSorting
{
    public interface IValue
    {
        int Value { get; set; }
    }

    public class Item : IValue, IComparable<Item>
    {
        public int Value { get; set; }

        public int CompareTo(Item other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Value.CompareTo(other.Value);
        }
    }

    public static class ComparableExtension
    {
        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }

        public static bool IsGreaterThan<T>(this T @this, T other) where T : IComparable<T>
        {
            return @this.CompareTo(other) > 0;
        }

        public static bool IsLessThan<T>(this T @this, T other) where T : IComparable<T>
        {
            return @this.CompareTo(other) < 0;
        }

        public static bool IsLessThanAndEqual<T>(this T @this, T other) where T : IComparable<T>
        {
            return @this.CompareTo(other) < 0;
        }
    }

    public class AbstractItemList<T> where T : IComparable<T>, IValue
    {
        private IList<T> _items;

        public AbstractItemList(IList<T> items)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            _items = items;
        }

        public void QuickSort()
        {
            QuickSort(_items, 0, _items.Count - 1);
        }

        private void QuickSort(IList<T> list, int low, int high)
        {
            if (low >= high)
                return;

            int mid = Partition(list, low, high);
            QuickSort(list, low, mid - 1);
            QuickSort(list, mid + 1, high);
        }

        private int Partition(IList<T> list, int low, int high)
        {
            int pivot = high;


            int rightmost = high - 1;
            int leftmost = low - 1;
            for (int i = low; i <= rightmost; i++)
            {
                if (list[i].IsLessThanAndEqual(list[pivot]))
                {
                    leftmost++;
                    list.Swap(leftmost, i);
                }
            }

            int mid = leftmost + 1;
            list.Swap(mid, high);
            return mid;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in _items)
            {
                stringBuilder.Append(item.Value).Append("-");
            }

            return stringBuilder.ToString();
        }
    }


    public class ItemList : AbstractItemList<Item>
    {
        public ItemList(IList<Item> items) : base(items)
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Item> items = new List<Item>()
            {
                new Item() {Value = 34},
                new Item() {Value = 54},
                new Item() {Value = 23},
                new Item() {Value = 56},
                new Item() {Value = 23},
                new Item() {Value = 33},
                new Item() {Value = 34},
            };
            ItemList itemList = new ItemList(items);
            itemList.QuickSort();
            Console.WriteLine(itemList);
        }
    }
}